from django.db import models

PROGRESS_STATE = (
    (0, 'Open'),
    (1, 'In Progress'),
    (2, 'Done'),
)


class Task(models.Model):
    """
    Defines a Task object which contains :
        1- Title
        2- Description
        3- State
    """

    title = models.CharField(max_length=40)
    description = models.CharField(max_length=300)
    state = models.IntegerField(choices=PROGRESS_STATE)
