from rest_framework import serializers

from ..models import Task


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = Task
        fields = ('id', 'title', 'description', 'state')
