from rest_framework import generics,viewsets

from apps.task.api.serializer import TaskSerializer
from apps.task.models import Task


class TaskViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Task to be viewed or edited.
    """
    serializer_class = TaskSerializer

    def get_queryset(self):

        return Task.objects.all()


class TaskViewSetCustom(generics.ListAPIView):
    """
    API endpoint that allows Task to be viewed or edited.
    """
    serializer_class = TaskSerializer

    def get_queryset(self):
        state = self.kwargs['state']
        return Task.objects.filter(state=state)
