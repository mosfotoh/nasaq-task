var axios = require('axios');



class TaskService{

    static loadTasks(state,callback) {
        axios.get('/api/task/list/'+state).then(response => {
            callback && callback(response.data);
        }).catch(error => {
            console.log(error);
        });
    }


    static createTask(task, callback) {
        axios.post('/api/task/', task).then( response => {
            callback && callback(response);
        }).catch( error => {
            console.log(error);
        });
    }

    static updateTask(task, callback) {
        axios.put('/api/task/'+task.id+'/', task).then( response => {
            callback && callback(response);
        }).catch( error => {
            console.log(error);
        });
    }



};

export default TaskService;
