import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import Table, {TableBody, TableCell, TableHead, TableRow} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import TaskService from "./service/task.service";
import CreateTaskDialog from "./task.dialog.component";

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
});


class TaskViewTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
        this.loadData();
    }

    loadData() {
        let st = "";

        TaskService.loadTasks(this.props.state, (data) => {
            if (this.props.state === 0) {
                st = "New"
            }
            if (this.props.state === 1) {
                st = "In Progress"
            }
            if (this.props.state === 2) {
                st = "Done"
            }
            this.setState({
                data: data,
                statues: st,
            });
        });
    }

    componentWillReceiveProps(props) {
        this.loadData();
    }


    render() {
        const {classes} = this.props;
        return (

            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>New Task</TableCell>
                            <TableCell>Description</TableCell>
                            <TableCell>State</TableCell>
                            <TableCell>Edit</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.data.map(task => {
                            return (
                                <TableRow key={task.id}>
                                    <TableCell>{task.title}</TableCell>
                                    <TableCell>{task.description}</TableCell>
                                    <TableCell>{this.state.statues}</TableCell>
                                    <TableCell><CreateTaskDialog task={task} onClose={() => {
                                        this.loadData()
                                    }}/></TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </Paper>
        );
    }


}

TaskViewTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TaskViewTable);