import React from 'react';
import {withStyles} from 'material-ui/styles';
import TaskTabsComponent from './task.tabes.component'
const styles = {
    root: {
        flexGrow: 1,
    },
};

class MainTaskComponent extends React.Component {
    state = {};


    render() {
        const {classes} = this.props;

        return (
            <div className={classes.root}>
                <TaskTabsComponent/>

            </div>
        );
    }
}


export default withStyles(styles)(MainTaskComponent);
