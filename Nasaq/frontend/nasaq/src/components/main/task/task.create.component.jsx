import React from 'react';
import {withStyles} from 'material-ui/styles';
import CreateTaskDialog from "./task.dialog.component";
import TaskViewTable from "./task.view.component";

const styles = {
    root: {
        flexGrow: 1,
    },
};

class CrateTaskComponent extends React.Component {
    state = {update: 1};


    render() {
        const {classes} = this.props;

        return (
            <div className={classes.root}>
                <CreateTaskDialog open={true} onClose={() => {
                    this.setState({
                        update: this.state.update + 1
                    })
                }}/>
                <TaskViewTable state={0} update={this.state.update}/>


            </div>
        );
    }
}


export default withStyles(styles)(CrateTaskComponent);
