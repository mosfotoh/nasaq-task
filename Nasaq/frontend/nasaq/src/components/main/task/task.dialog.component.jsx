import React from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {DialogActions, DialogContent, DialogTitle,} from 'material-ui/Dialog';
import {InputLabel} from 'material-ui/Input';
import {MenuItem} from 'material-ui/Menu';
import {FormControl} from 'material-ui/Form';
import Select from 'material-ui/Select';
import TaskService from './service/task.service';

export default class CreateTaskDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            title: "",
            description: "",
            taskState: "",
            task: {},
            mode: 'create',

        };




    }

    componentWillReceiveProps(props){
        if (props.task) {

            this.setState({
                mode: 'edit',
                title: this.props.task.title,
                description: this.props.task.description,
                taskState: this.props.task.state,
            });
        }
    }

    editTask = (task) => {

        TaskService.updateTask(task, (data) => {

        });
    };

    handleClickOpen = () => {
        this.setState({
            open: true
        });
    };

    handleClose = () => {
        this.setState({
            open: false
        });

        this.props.onClose && this.props.onClose();
    };
    handleSave = () => {
        const task = {
            title: this.state.title,
            description: this.state.description,
            state: this.state.taskState
        };

        if (this.state.mode === 'edit') {
            task['id'] = this.props.task.id;

            this.editTask(task)

        } else {
            TaskService.createTask(task, () => {
/*                this.setState({
                    title: "",
                    description: "",
                    taskState: "",
                    mode: 'create'
                });*/

            });

        }


        this.handleClose();

    };
    handleChange = (event) => {
        this.setState({[event.target.name]: event.target.value,});
    };


    render() {
        return (
            <div>
                <Button onClick={this.handleClickOpen}>{this.state.mode}</Button>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Create Task</DialogTitle>
                    <DialogContent>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="title"
                            label="Title"
                            name="title"
                            value={this.state.title}
                            onChange={this.handleChange}
                            fullWidth
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            id="description"
                            label="Description"
                            name="description"
                            value={this.state.description}
                            type="description"
                            onChange={this.handleChange}
                            fullWidth
                        />

                        <FormControl>
                            <InputLabel htmlFor="age-simple">State</InputLabel>
                            <Select
                                value={this.state.taskState}
                                onChange={(event) => {

                                    this.setState({
                                        taskState: event.target.value
                                    });

                                }}
                            >
                                <MenuItem value={0}>New</MenuItem>
                                <MenuItem value={1}>In progress</MenuItem>
                                <MenuItem value={2}>Done</MenuItem>
                            </Select>
                        </FormControl>

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleSave} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}