import React, {Component} from 'react';
import './App.css';
import HeaderComponent from './components/core/header/core.header.component'
import MainTaskComponent from './components/main/task/main.task.component'

class App extends Component {
    render() {
        return (
            <div className="App">
                <HeaderComponent/>
                <MainTaskComponent/>
            </div>
        );
    }
}

export default App;
